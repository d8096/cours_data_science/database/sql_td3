# sql_td3

## Contexte

En complément de la base existante, on souhaite enregistrer deux nouveaux types d'informations : les mesures issues d'appareils comme les respirateurs, moniteurs de fréquence cardiaque, etc. et les administrations de médicaments.

Ces deux informations peuvent être collectées dans n'importe quelles unités de soins.

On souhaite conserver la provenance de la mesure (la machine qui l'a réalisée). Il y a une liste de toutes les machines. La mesure est caractérisée par une valeur, une unité et une date.

L'administration d'un médicament est caractérisé par le médicament (il existe une liste de tous les médicaments), la dose, l'unité de la dose, la concentration et l'unité de la concentration, la date à laquelle le médicament est administré. Il n'y a pas toujours de concentration.

## Exercice

1 - Modéliser les tables, colonnes et contraites pour stocker ces nouvelles informations.
2 - Proposer les scripts pour les créer dans la base de données.
3 - Insérer plusieurs enregistrements cohérents.